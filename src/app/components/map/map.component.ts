import { Component, OnInit } from '@angular/core';

import { HttpCaller }   from '../../services/http.caller';
import { map, heatmap } from '../../constants/map';

import { IPoint }     from '../../interfaces/i-point';
import { Coordinate } from '../../types/coordinate';

@Component({
    selector   : 'map',
    templateUrl: './map.component.html',
    styleUrls  : ['./map.component.scss'],
})
export class MapComponent implements OnInit {

    public map = map;

    constructor(private http: HttpCaller) {
    }

    public ngOnInit(): void {
        this.http.getMockPoints().subscribe(points => {
            this.renderMap(points);
        });
    }

    private renderMap(points: Array<IPoint>): void {
        window.ymaps.ready([heatmap]).then(function init() {
            const coordinates = new Array<Coordinate>();
            const myMap       = new window.ymaps.Map(map, {
                center  : [55.733835, 37.588227],
                zoom    : 11,
                controls: [],
            });

            points.forEach((point: IPoint) => {
                const { coordinates: coords } = point.Cells.geoData;

                coordinates.push([coords[1], coords[0]])
            });

            const heatmap = new window.ymaps.Heatmap(coordinates, {
                radius             : 15,
                dissipating        : false,
                opacity            : 0.8,
                intensityOfMidpoint: 0.2,
                gradient           : {
                    0.1: 'rgba(128, 255, 0, 0.7)',
                    0.2: 'rgba(255, 255, 0, 0.8)',
                    0.7: 'rgba(234, 72, 58, 0.9)',
                    1.0: 'rgba(162, 36, 25, 1)',
                },
            });

            heatmap.setMap(myMap);
        });
    }
}
