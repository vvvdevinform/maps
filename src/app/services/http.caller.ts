import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';

import { IPoint } from '../interfaces/i-point';

@Injectable({ providedIn: 'root' })
export class HttpCaller {

    private jsonURL = 'assets/map.json';

    constructor(private http: HttpClient) {
    }

    public getMockPoints(): Observable<Array<IPoint>> {
        return this.http.get<Array<IPoint>>(this.jsonURL);
    }
}
