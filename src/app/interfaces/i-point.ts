import { Coordinate } from '../types/coordinate';

export interface IPoint {

    Id: string,
    Number: number,
    Cells: ICell
}

interface ICell {

    global_id: number,
    ID: string,
    AdmArea: string,
    District: string,
    Address: string,
    OVDAddress: string,
    OVDPhone: Array<IOvdPhone>,
    geoData: IGeo,
}

interface IGeo {

    type: string;
    coordinates: Coordinate;
}

interface IOvdPhone {

    PhoneOVD: string;
}
